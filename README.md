# IVR HomeTask

Middleware web service that allow IVR application to
communicate with Stripe payment processor and return the results of the
transaction to the IVR application.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

Django (2.2.4)
DjangoRestFramework (3.10.2)

Mysql 5.7
 * After mysql is installed verify that libmysqlclient-dev is installed else istall using : sudo apt-get install python3-dev libmysqlclient-dev


mysqlclient (1.44)

* note : You must create a database named taskdb, and user called my_admin
ELSE you must create and change the database credentials
#### Install Requirements and Migration
```bash
pip install -r requirements.txt
python manage.py migrate

```

## Running software

```bash
python manage.py runserver
```

## Testing API
If the project is running, you can test it sending the next JSON via POST to http://localhost:9000/payment/token/ :
```bash
{
"cc_num": "4242424242424242",
"cvv": "123",

//First 2 digits -> month , Last 2 digits -> year
"exp_date": "1219", 

"trans_id": "321654351687",
"amount": "1200"
}
```
### Cards for Testing
* 4242424242424242
* 4000056655665556
* 5555555555554444
* 2223003122003222    
* 5200828282828210

find more [here](https://stripe.com/docs/testing#cards)
## Documentation
In the docs folder run the following commands. And go to http://localhost:9000/ in your browser.

```bash
sphinx-build -b html source build
cd build
python -m http.server 9000
```

## Authors

* **Kenneth Alvarez** -  [kenalv](https://gitlab.com/kenalv)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details