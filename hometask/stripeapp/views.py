from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response
from rest_framework import generics
import json

from stripeapp.models import Transaction
from stripeapp.serializers import DetailedTransactionSerializer


class PaymentRequestView(generics.GenericAPIView):
    """
    This class handle the request after the middleware is done. You can access the data set in the request.
    """
    def post(self, request):
        transaction_pk = request.transaction_pk
        detailed_transaction_object = DetailedTransactionSerializer(Transaction.objects.get(pk=transaction_pk))

        return HttpResponse(f"request_id: {detailed_transaction_object['request_id']['id']} , transaction : {detailed_transaction_object['request_id']['trans_id']}, card_num: {detailed_transaction_object['request_id']['cc_num']},amount: {detailed_transaction_object['request_id']['amount']},charge_id: {detailed_transaction_object['response_id']['id']},currency: {detailed_transaction_object['response_id']['currency']}, status: {detailed_transaction_object['response_id']['status']}",content_type = 'text/plain')
