from django.urls import path
from .views import PaymentRequestView

urlpatterns = [
    path('token/', PaymentRequestView.as_view(), name="payment"),
]
